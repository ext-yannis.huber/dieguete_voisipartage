# VoisiPartage - DieGuete

Voici les contributions de chacun des membres:

- Adrian Buntschu: add item, modify item

- Loïc Freiburghaus: object list, object list item, homepage, merging

- Yannis Huber: toolbar, searchbar, search results, routing, merging

- Yael Iseli: détails d'un objet, responsive toolbar

- Stefan Pahud : search page, search list categorie
