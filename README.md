# VoisiPartage - DieGuete
Dans le cadre du cours IHM2, nous développons une application Web dans le but d'appliquer les concepts vu en cours. Notre groupe a développé un projet dans le but de simplifier le partage d'objets.

## Membres
Adrian Buntschu

Loïc Freiburghaus

Yannis Huber

Yael Iseli

Stefan Pahud

## Concept
Bien souvent, nous avons besoin d'un objet précis pour un usage unique ou au contraire, nous le possédons mais ne l'utilisons que très rarement. L'application web **VoisiPartage** permet aux personnes d'une même commune de gérer le prêt et l'emprunt d'objets entre eux. Elle permet aux utilisateurs de mettre à disposition un objet ou de trouver ce dont ils ont besoin. Toute personne est libre de vouloir uniquement prêter ou emprunter. Chaque prêt/emprunt est réglementé mais peut tout de même être défini différemment entre les deux personnes à travers un *chat* ou de vive voix. L'application est aussi disponible pour les mobiles.

L'idée de ce projet est de mettre le sens du partage et de la vie communautaire au centre du village dans le but de renforcer les liens et de réduire le gaspillage.


## React

Le site Web développé dans le cadre de ce cours utilise React comme *framework frontend*. Pour lancer l'application, il faut suivre les étapes suivantes depuis le répertoire Frontend:

> $ yarn install

> $ yarn start
