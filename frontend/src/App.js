import React from "react";
import "./App.css";
import Homepage from "./components/Homepage";
import NavigationBar from "./components/toolbar/NavigationBar";
import AddItem from "./components/AddItem";
import ModifyItem from "./components/ModifyItem";
import SearchItem from "./components/SearchPage";

import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import { ThemeProvider } from "@material-ui/core/styles";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MyObject from "./components/objectDetails/MyObject";
import MyItems from "./components/MyItems";
import { useMediaQuery } from "react-responsive";
import ResponsiveNavigationBar from "./components/toolbar/ResponsiveNavigationBar";

const theme = createMuiTheme({
  spacing: 2,
  palette: {
    primary: {
      main: "#689f38"
    },
    secondary: {
      main: "#d32f2f"
    }
  },
  typography: {
    fontFamily: [
      "Karla",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(",")
  }
});

function App() {
  const isSmall = useMediaQuery({ query: "(max-width: 1000px)" });
  return (
    <ThemeProvider theme={theme}>
      <Router>
        {isSmall ? <ResponsiveNavigationBar /> : <NavigationBar />}
        <Switch>
          <Route path="/details/:id" component={MyObject} />
          <Route path="/myitems" component={MyItems} />
          <Route path="/additem">
            <AddItem />
          </Route>
          <Route path="/searchitem">
            <SearchItem />
          </Route>
          <Route path="/modifyitem/:id" component={ModifyItem}></Route>
          <Route path="/">
            <Homepage />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
