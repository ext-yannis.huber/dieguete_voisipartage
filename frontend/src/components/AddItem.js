import React from "react";
import "../App.css";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import ChipInput from "material-ui-chip-input";

import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import SelectImages from "./SelectImages.js";
import { Link } from "react-router-dom";

const styles = {
  textField: {
    width: "500px"
  },
  button: {
    margin: "15px"
  },
  title: {
    margin: "20px"
  },
  chipInput: {
    margin: "15px",
    width: "500px"
  },
  root: {
    display: "grid",
    justifyItems: "center"
  }
};

class AddItem extends React.Component {
  constructor(props) {
    super(props);

    this.handelNameChange = this.handelNameChange.bind(this);
    this.handelDescriptionChange = this.handelDescriptionChange.bind(this);
    this.addChip = this.addChip.bind(this);
    this.removeChip = this.removeChip.bind(this);

    this.state = {
      canBeAdded: false,
      itemName: "",
      itemDescription: "",
      itemImage: "",
      itemTags: []
    };
  }

  handelNameChange(event) {
    const comp = this;
    const value = event.target.value;
    this.setState(state => {
      return {
        itemName: value,
        canBeAdded:
          value !== "" &&
          comp.state.itemDescription !== "" &&
          comp.state.itemTags !== ""
      };
    });
  }

  handelDescriptionChange(event) {
    const comp = this;
    const value = event.target.value;
    this.setState(state => {
      return {
        itemDescription: value,
        canBeAdded:
          comp.state.itemName !== "" &&
          value !== "" &&
          comp.state.itemTags !== ""
      };
    });
  }

  addChip(chip) {
    const comp = this;
    var tags = this.state.itemTags;
    tags.push(chip);
    this.setState(state => {
      return {
        itemTags: tags,
        canBeAdded:
          comp.state.itemName !== "" && comp.state.itemDescriptionlue !== ""
      };
    });
  }

  removeChip(pos) {
    const comp = this;
    var tags = this.state.itemTags;
    tags.splice(pos, 1);
    this.setState(state => {
      return {
        itemTags: tags,
        canBeAdded:
          (comp.state.itemName !== "" && comp.state.itemDescriptionlue !== "",
          tags.length !== 0)
      };
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Typography className={classes.title} variant="h3" color="primary">
          Nouvel Objet
        </Typography>
        <form noValidate autoComplete="off">
          <TextField
            required
            id="item-title"
            label="Nom"
            variant="outlined"
            margin="normal"
            onChange={this.handelNameChange}
            className={classes.textField}
          />
          <br />
          <TextField
            required
            id="item-description"
            multiline
            label="Description"
            variant="outlined"
            margin="normal"
            onChange={this.handelDescriptionChange}
            className={classes.textField}
          />
          <br />
          <SelectImages
            images={[]}
            onImagesChange={images => {
              console.log("Images selection change: " + images);
            }}
          />
          <ChipInput
            required
            label="Tags"
            className={classes.chipInput}
            value={this.state.itemTags}
            onAdd={chip => this.addChip(chip)}
            onDelete={(chip, index) => this.removeChip(index)}
          />
          <br />
        </form>
        <Link to="/" style={{ textDecoration: "none" }}>
          <Button
            disabled={!this.state.canBeAdded}
            variant="contained"
            color="primary"
            startIcon={<SaveIcon />}
            className={classes.button}
            style={{ textDecoration: "none" }}
          >
            Ajouter
          </Button>
        </Link>
        <Link to="/" style={{ textDecoration: "none" }}>
          <Button
            variant="contained"
            color="secondary"
            startIcon={<CancelIcon />}
            className={classes.button}
          >
            Annuler
          </Button>
        </Link>
      </div>
    );
  }
}

export default withStyles(styles)(AddItem);
