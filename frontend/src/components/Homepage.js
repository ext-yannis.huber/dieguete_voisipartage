import React from "react";
import ObjectList from "./objectList/ObjectList";
import ObjectData from "../data/objects";
import { withStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import logo from "../logo-VoisiPartage.png";

class Homepage extends React.Component {
  static style = {
    root: {
      padding: "20px 15%",
      display: "grid"
    },
    centeredContent: {
      justifySelf: "center"
    },
    bottomMargin: {
      marginBottom: "40px"
    },
    catChip: {
      display: "inline"
    }
  };

  /**
   * Shuffles array in place.
   * @param {Array} a items An array containing the items.
   * Source: https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
   */
  shuffle = a => {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
    }
    return a;
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.root}>
          <img src={logo} alt="Logo" className={classes.centeredContent} />
          <Typography variant="h4" className={classes.centeredContent}>
            Bienvenue sur VoisiPartage !
          </Typography>
          <Typography
            variant="h6"
            className={`${classes.centeredContent} ${classes.bottomMargin}`}
          >
            Commencez tout de suite à prêter et emprunter des objets dans votre
            voisinage.
          </Typography>
          <Typography variant="h5">Recommandé pour vous :</Typography>
          <Typography variant="subtitle1" style={{ marginBottom: "20px" }}>
            Les objets qui pourraient vous intéresser...
          </Typography>
          <div style={{ width: "100%", height: "300px", marginBottom: "40px" }}>
            <ObjectList
              data={this.shuffle(ObjectData).slice(0, 10)}
              logged={false}
              horizontally={true}
            />
          </div>
          <Typography variant="h5">Tendances actuelles :</Typography>
          <Typography variant="subtitle1" style={{ marginBottom: "20px" }}>
            Voici les objets très demandés près de chez vous...
          </Typography>
          <div style={{ width: "100%", height: "300px", marginBottom: "40px" }}>
            <ObjectList
              data={this.shuffle(ObjectData).slice(0, 10)}
              logged={false}
              horizontally={true}
            />
          </div>
          <Typography variant="h5" style={{ marginBottom: "20px" }}>
            Catégorie recommandée : Jardinage
          </Typography>
          <div style={{ width: "100%", height: "300px", marginBottom: "40px" }}>
            <ObjectList
              data={this.shuffle(ObjectData).slice(0, 10)}
              logged={false}
              horizontally={true}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default withStyles(Homepage.style)(Homepage);
