import React from "react";
import "../App.css";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import ChipInput from "material-ui-chip-input";

import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import SelectImages from "./SelectImages.js";
import { Link } from "react-router-dom";

import data from "../data/objects";

const styles = {
  textField: {
    width: "500px"
  },
  button: {
    margin: "15px"
  },
  title: {
    margin: "20px"
  },
  chipInput: {
    margin: "15px",
    width: "500px"
  },
  root: {
    display: "grid",
    justifyItems: "center"
  }
};

class ModifyItem extends React.Component {
  constructor(props) {
    super(props);

    this.handelNameChange = this.handelNameChange.bind(this);
    this.handelDescriptionChange = this.handelDescriptionChange.bind(this);
    this.hasChanged = this.hasChanged.bind(this);
    this.addChip = this.addChip.bind(this);
    this.removeChip = this.removeChip.bind(this);

    const params = this.props.match.params;
    const object = data.find(e => e.id === params.id);

    this.state = {
      isModified: false,
      areImagesModified: false,
      areTagsModified: false,
      itemName: object.title,
      itemDescription: object.description,
      itemImage: "",
      itemTags: object.categories
    };
  }

  handelNameChange(event) {
    const comp = this;
    const value = event.target.value;
    this.setState(state => {
      return {
        itemName: value,
        isModified: this.hasChanged(
          value,
          comp.state.itemDescription,
          comp.state.itemTags
        )
      };
    });
  }

  handelDescriptionChange(event) {
    const comp = this;
    const value = event.target.value;
    this.setState(state => {
      return {
        itemDescription: value,
        isModified: this.hasChanged(
          comp.state.itemName,
          value,
          comp.state.itemTags
        )
      };
    });
  }

  addChip(chip) {
    var tags = this.state.itemTags;
    tags.push(chip);
    this.setState(state => {
      return {
        itemTags: tags,
        areTagsModified: true
      };
    });
  }

  removeChip(pos) {
    var tags = this.state.itemTags;
    tags.splice(pos, 1);
    this.setState(state => {
      return {
        itemTags: tags,
        areTagsModified: true
      };
    });
  }

  hasChanged(modName, modDescription, modTags) {
    const params = this.props.match.params;
    const object = data.find(e => e.id === params.id);
    return !(modName === object.title && modDescription === object.description);
  }

  render() {
    const params = this.props.match.params;
    const object = data.find(e => e.id === params.id);
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Typography variant="h3" className={classes.title} color="primary">
          Modifier Objet
        </Typography>
        <form noValidate autoComplete="off">
          <TextField
            required
            id="item-title"
            label="Nom"
            variant="outlined"
            margin="normal"
            onChange={this.handelNameChange}
            className={classes.textField}
            value={this.state.itemName}
          />
          <br />
          <TextField
            required
            id="item-description"
            multiline
            label="Description"
            variant="outlined"
            margin="normal"
            onChange={this.handelDescriptionChange}
            className={classes.textField}
            value={this.state.itemDescription}
          />
          <br />
          <SelectImages
            images={object.imageUrl}
            onImagesChange={images => {
              this.setState({ areImagesModified: true });
              console.log("Images selection change: " + images);
            }}
          />
          <ChipInput
            required
            label="Tags"
            className={classes.chipInput}
            value={this.state.itemTags}
            onAdd={chip => this.addChip(chip)}
            onDelete={(chip, index) => this.removeChip(index)}
          />
          <br />
        </form>
        <Link to={`/myitems`}>
          <Button
            disabled={
              !(
                this.state.isModified ||
                this.state.areImagesModified ||
                this.state.areTagsModified
              )
            }
            variant="contained"
            color="primary"
            startIcon={<SaveIcon />}
            className={classes.button}
          >
            Sauvgarder
          </Button>
        </Link>
        <Link to={`/myitems`}>
          <Button
            variant="contained"
            color="secondary"
            startIcon={<DeleteIcon />}
            className={classes.button}
          >
            Effacer
          </Button>
        </Link>
        <Link to={`/myitems`}>
          <Button
            variant="contained"
            color="secondary"
            startIcon={<CancelIcon />}
            className={classes.button}
            style={{ textDecoration: "none" }}
          >
            Annuler
          </Button>
        </Link>
      </div>
    );
  }
}

export default withStyles(styles)(ModifyItem);
