import React from "react";
import ObjectList from "./objectList/ObjectList";
import ObjectData from "../data/objects";
import Typography from "@material-ui/core/Typography";

class MyItems extends React.Component {
  render() {
    return (
      <div style={{ width: "70%", margin: "auto" }}>
        <Typography variant="h5" style={{ margin: "20px" }}>
          Objets que vous empruntez:
        </Typography>

        <div style={{ width: "100%", height: "100vh" }}>
          <ObjectList
            data={ObjectData.slice(0, 30)}
            logged={true}
            horizontally={false}
          />
        </div>
      </div>
    );
  }
}
export default MyItems;
