import React from "react";
import ObjectList from "./objectList/ObjectList";
import Categories from "./searchList/Categories";
import { Grid, Box, Container } from "@material-ui/core";
import Calendar from "react-calendar";
import Tags from "../data/categories";
import Tmp from "../data/searchObjectDummy";

const tags = Tags;
const tmp = Tmp;

class SearchPage extends React.Component {
  render() {
    return (
      <div style={{ padding: 20 }}>
        <Box mx="auto">
          <Grid
            container
            direction="row"
            justify="center"
            spacing={0}
            alignItems="flex-start"
          >
            <Grid item xs={4}>
              <Box mx="auto" style={{ padding: 10 }}>
                <Container maxWidth="sm">
                  <Calendar />
                  <h3>Filtres :</h3>
                  <div style={{ width: "100%", height: "2px" }}>
                    <Categories data={tags} />
                  </div>
                </Container>
              </Box>
            </Grid>

            <Grid item xs={8}>
              <Container>
                <h3>Résultats :</h3>
                <ObjectList data={tmp} logged={false} horizontally={false} />
              </Container>
            </Grid>
          </Grid>
        </Box>
      </div>
    );
  }
}

export default SearchPage;
