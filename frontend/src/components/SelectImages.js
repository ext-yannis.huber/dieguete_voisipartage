import React from "react";
import "../App.css";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";

import CloudUploadIcon from "@material-ui/icons/CloudUpload";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";

const styles = {
  button: {
    margin: "15px"
  },
  title: {
    margin: "20px"
  },
  mediaCard: {
    height: "300px",
    width: "450px"
  },
  card: {
    height: "350px",
    width: "450px",
    margin: "10px",
    display: "inline-block"
  },
  media: {
    height: 140
  },
  root: {}
};

class SelectImages extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: this.props.images
    };

    this.addImage = this.addImage.bind(this);
    this.removeImage = this.removeImage.bind(this);
  }

  addImage(newImage) {
    newImage = "https://dummyimage.com/600x400/000/fff";
    var imgList = this.state.images;
    imgList.push(newImage);
    this.setState({ images: imgList });
    this.props.onImagesChange(imgList);
  }

  removeImage(pos) {
    var imgList = this.state.images;
    imgList.splice(pos, 1);
    this.setState({ images: imgList });
    this.props.onImagesChange(imgList);
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <input
          style={{ display: "none" }}
          accept="image/*"
          id="contained-button-file"
          type="file"
          multipletype="file"
        />
        <label htmlFor="contained-button-file">
          <Button
            onClick={this.addImage}
            variant="outlined"
            color="primary"
            startIcon={<CloudUploadIcon />}
            component="span"
          >
            Selectioner Images
          </Button>
        </label>
        <br />
        <div>
          {this.state.images.map((image, pos) => {
            return (
              <Card className={classes.card}>
                <CardMedia image={image} className={classes.mediaCard} />
                <CardActions>
                  <Button
                    onClick={e => {
                      this.removeImage(pos);
                    }}
                    startIcon={<DeleteIcon />}
                    variant="outlined"
                    size="small"
                    color="secondary"
                  >
                    Effacer
                  </Button>
                </CardActions>
              </Card>
            );
          })}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(SelectImages);
