import Chip from "@material-ui/core/Chip";
import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  chip: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(0.5)
    }
  }
}));

const Chips = ({ categories }) => {
  const classes = useStyles();

  return (
    <div className={classes.chip}>
      {categories.map((chip, id) => {
        return <Chip key={id} label={chip} />;
      })}
    </div>
  );
};

export default Chips;
