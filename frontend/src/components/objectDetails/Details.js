import React from "react";
import { Button, makeStyles, Typography, Chip } from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import ClearIcon from "@material-ui/icons/Clear";
import Chips from "./Chips";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    margin: "0 auto 30px"
  },
  chip: {
    margin: "10px 0 0 0",
    padding: "0 0 0 10px"
  },
  typo: {
    margin: "10px 0 0 0 "
  },
  italic: {
    margin: "10px 0 0 0 ",
    fontStyle: "italic",
    color: "gray"
  },
  isAvailable: {
    margin: "10px 0 0 0 ",
    color: "lightgreen"
  },
  isNotAvailable: {
    margin: "10px 0 0 0 ",
    color: "red"
  }
}));

export default function Details({ obj }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h4" component="h3">
        {obj.title}
      </Typography>
      <Typography className={classes.chip}>
        <Chips categories={obj.categories} />
      </Typography>
      <Typography component="p" className={classes.typo}>
        {obj.description}
      </Typography>

      <Typography component="p" className={classes.typo}>
        <Chip
          label={
            obj.availability.isAvailable ? (
              <DoneIcon fontSize={"small"} />
            ) : (
              <ClearIcon fontSize={"small"} />
            )
          }
          size="small"
          color={obj.availability.isAvailable ? "primary" : "secondary"}
        />

        <Typography
          component="span"
          style={{ paddingLeft: "10px" }}
          color={obj.availability.isAvailable ? "primary" : "secondary"}
        >
          {obj.availability.isAvailable ? "Disponible !" : "Non disponible !"}
        </Typography>
      </Typography>
      {obj.availability.isAvailable ? (
        <Typography component="p" className={classes.typo}>
          <div>
            Du {obj.availability.startDate} jusqu'au {obj.availability.endDate}
          </div>
        </Typography>
      ) : (
        ""
      )}
      <Button variant="contained" className={classes.typo}>
        Envoyer une demande
      </Button>
      <Typography component="p" className={classes.italic}>
        Important : votre demande ne sera validée qu'une fois que le
        propriétaire ait accepté le prêt.
      </Typography>
    </div>
  );
}
