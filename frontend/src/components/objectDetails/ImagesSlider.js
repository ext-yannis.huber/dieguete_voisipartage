import React from "react";

import Slider from "infinite-react-carousel";
import { makeStyles } from "@material-ui/core";
import "../../App.css";

const useStyles = makeStyles(theme => ({
  row: {
    alignItems: "center"
  },
  root: {
    width: "100%",
    margin: "0 auto"
  }
}));

const ImagesSlider = ({ imageUrl }) => {
  const classes = useStyles();
  const settings = {
    dots: true,
    adaptiveHeight: true
  };

  return (
    <div className={classes.root}>
      <Slider {...settings}>
        {imageUrl.map((img, id) => {
          return (
            <div className={classes.row}>
              <img alt="img" key={id} src={img} className="img" />
            </div>
          );
        })}
      </Slider>
    </div>
  );
};

export default ImagesSlider;
