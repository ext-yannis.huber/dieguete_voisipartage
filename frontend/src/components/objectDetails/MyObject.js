import React from "react";
import { Grid } from "@material-ui/core";
import Owner from "./Owner";
import ImagesSlider from "./ImagesSlider";
import Details from "./Details";
import SimpleBreadCrumb from "./SimpleBreadcrumb";
import data from "../../data/objects";
import { withStyles } from "@material-ui/styles";

class MyObject extends React.Component {
  static styles = {
    grid: {
      flexGrow: 1,
      direction: "row",
      justify: "center",
      padding: 20
    },
    row: {
      width: "100%"
    },
    root: {
      height: "100vh",
      width: "70%",
      margin: "0 auto",
      padding: "5px"
    }
  };

  render() {
    const {
      classes,
      match: { params }
    } = this.props;

    const object = data.find(e => e.id === params.id);
    console.log(object);
    console.log(params.id);
    return (
      <div className={classes.root}>
        <SimpleBreadCrumb title={object.title} />
        <Grid container className={classes.grid} spacing={8}>
          <Grid item xs={12} sm={12} md={6}>
            <div className={classes.row}>
              <ImagesSlider imageUrl={object.imageUrl} />
            </div>

            <Owner owner={object.owner} />
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
            <Details obj={object} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(MyObject.styles)(MyObject);
