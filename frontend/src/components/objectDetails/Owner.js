import React from "react";
import { PermIdentity } from "@material-ui/icons";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import { Typography } from "@material-ui/core";

export default function Owner({ owner }) {
  return (
    <div style={{ width: "100%", margin: "0 auto", paddingTop: "20px" }}>
      <Typography variant="h5" component="h5">
        Prêteur
      </Typography>
      <List>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <PermIdentity />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={owner.lastName} secondary={owner.firstName} />
        </ListItem>
      </List>
    </div>
  );
}
