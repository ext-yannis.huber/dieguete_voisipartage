import React from "react";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import { Link } from "react-router-dom";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  breadcrumb: {
    padding: theme.spacing(3, 2)
  }
}));

export default function SimpleBreadcrumb({ title }) {
  const classes = useStyles();

  return (
    <Breadcrumbs
      separator={<NavigateNextIcon fontSize="small" />}
      aria-label="breadcrumb"
      className={classes.breadcrumb}
    >
      <Link color="inherit" to="/">
        Objets à prêter
      </Link>
      <Typography color="textPrimary">{title}</Typography>
    </Breadcrumbs>
  );
}
