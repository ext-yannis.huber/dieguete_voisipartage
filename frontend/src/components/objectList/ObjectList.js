import React from "react";
import GridList from "@material-ui/core/GridList";
import ObjectListItem from "./ObjectListItem";
import { withStyles } from "@material-ui/styles";

class ObjectList extends React.Component {
  static styles = {
    gridListHorizontally: {
      display: "flex",
      flexFlow: "column wrap",
      width: "100%",
      height: "100%",
      overflowX: "auto"
    },
    gridListVertically: {
      display: "flex",
      flexFlow: "row wrap",
      width: "100%",
      height: "100%",
      justifyContent: "center",
      overflowX: "auto"
    }
  };

  state = { objects: this.props.data };

  removeitem = e => {
    const { data } = this.props;
    data.splice(
      data.find(el => el.id === e.target.dataset.id),
      1
    );
    this.setState({ objects: data });
  };

  render() {
    const { classes, logged, horizontally } = this.props;
    let style;
    if (horizontally) {
      style = classes.gridListHorizontally;
    } else {
      style = classes.gridListVertically;
    }
    return (
      <GridList className={style}>
        {this.state.objects.map(object => (
          <ObjectListItem
            data={object}
            logged={logged}
            deleteCallback={this.removeitem}
          />
        ))}
      </GridList>
    );
  }
}
export default withStyles(ObjectList.styles)(ObjectList);
