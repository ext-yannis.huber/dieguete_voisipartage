import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/styles";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { Chip, Tooltip } from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import ClearIcon from "@material-ui/icons/Clear";
import { Link } from "react-router-dom";

class ObjectListItem extends React.Component {
  static styles = {
    card: {
      width: 270,
      margin: "10px"
    },
    media: {
      maxWidth: 270,
      height: 120
    },
    actions: {
      width: "100%",
      display: "grid",
      gridTemplateColumns: "1fr auto auto",
      justifyItems: "start",
      alignItems: "center"
    }
  };

  createEditionButtons = () => {
    const { data, deleteCallback } = this.props;
    return [
      <Tooltip title="Modifier">
        <Link to={`/modifyitem/${data.id}`}>
          <IconButton size="small" color="primary">
            <EditIcon />
          </IconButton>
        </Link>
      </Tooltip>,
      <Tooltip title="Supprimer">
        <IconButton
          size="small"
          color="secondary"
          data-id={data.id}
          onClick={deleteCallback}
        >
          <DeleteIcon />
        </IconButton>
      </Tooltip>
    ];
  };

  render() {
    const { data, classes, logged } = this.props;
    let button;

    button = (
      <div className={classes.actions}>
        <Tooltip
          title={data.availability.isAvailable ? "Disponible" : "Indisponible"}
        >
          <Chip
            label={
              data.availability.isAvailable ? (
                <DoneIcon fontSize={"small"} />
              ) : (
                <ClearIcon fontSize={"small"} />
              )
            }
            size="small"
            color={data.availability.isAvailable ? "primary" : "secondary"}
          />
        </Tooltip>

        {logged ? (
          this.createEditionButtons(data)
        ) : (
          <Link to={`/details/${data.id}`} style={{ textDecoration: "none" }}>
            <Button size="small" color="primary">
              Détails
            </Button>
          </Link>
        )}
      </div>
    );

    return (
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={data.imageUrl[0]}
          title={data.title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" noWrap={true}>
            {data.title}
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            noWrap={true}
          >
            {data.description}
          </Typography>
        </CardContent>

        <CardActions>{button}</CardActions>
      </Card>
    );
  }
}

export default withStyles(ObjectListItem.styles)(ObjectListItem);
