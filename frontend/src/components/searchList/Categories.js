import React from "react";
import { Grid, FormControlLabel, Checkbox } from "@material-ui/core";

class Categories extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <Grid>
        {data.map(tag => (
          <FormControlLabel control={<Checkbox />} label={tag} />
        ))}
      </Grid>
    );
  }
}
export default Categories;
