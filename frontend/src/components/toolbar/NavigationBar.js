import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import ToolbarGroup from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import SearchBar from "./search/SearchBar";
import { withStyles } from "@material-ui/styles";
import ToolbarButtons from "./account/ToolbarButtons";
import { Link } from "react-router-dom";

const styles = {
  searchBar: {
    float: "none",
    width: "50%",
    maxWidth: "500px",
    marginLeft: "auto",
    marginRight: "auto"
  },
  homeLink: {
    textDecoration: "none",
    color: "white"
  }
};

class NavigationBar extends React.Component {
  state = { isLoggedIn: false };

  render() {
    const { classes } = this.props;
    return (
      <AppBar position="static" style={{ padding: "0px 15%" }}>
        <Toolbar style={{ padding: "0" }}>
          <ToolbarGroup float="left" style={{ paddingLeft: "0" }}>
            <Link to="/" className={classes.homeLink}>
              <Typography variant="h5">VoisiPartage</Typography>
            </Link>
          </ToolbarGroup>

          <ToolbarGroup className={classes.searchBar}>
            <SearchBar />
          </ToolbarGroup>

          <ToolbarGroup float="right" style={{ paddingRight: "0" }}>
            <ToolbarButtons />
          </ToolbarGroup>
        </Toolbar>
      </AppBar>
    );
  }
}
export default withStyles(styles)(NavigationBar);
