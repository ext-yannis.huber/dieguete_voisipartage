import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import ToolbarGroup from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import { Link } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Drawer from "@material-ui/core/Drawer";

import Button from "@material-ui/core/Button";
import { Avatar } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DashboardIcon from "@material-ui/icons/Dashboard";
import MenuIcon from "@material-ui/icons/Menu";

const useStyles = makeStyles(theme => ({
  homeLink: {
    textDecoration: "none",
    color: "white"
  },

  btns: {
    color: "white"
  },

  drawer: {
    backgroundColor: "#689f38"
  }
}));

export default function ResponsiveNavigationBar() {
  const classes = useStyles();

  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });

  const toggleDrawer = (side, open) => event => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        <ListItem button>
          <ListItemIcon>
            <Avatar>AB</Avatar>
          </ListItemIcon>
          <ListItemText primary={"Albert !"} className={classes.btns} />
        </ListItem>
        <ListItem button className={classes.btns}>
          <ListItemIcon className={classes.btns}>
            <AddIcon />
          </ListItemIcon>
          <ListItemText primary={"Ajouter"} className={classes.btns} />
        </ListItem>
        <ListItem button className={classes.btns}>
          <ListItemIcon className={classes.btns}>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary={"Mes objets"} className={classes.btns} />
        </ListItem>
      </List>
    </div>
  );

  return (
    <AppBar position="static" style={{ padding: "0px 15%" }}>
      <Toolbar style={{ padding: "0" }}>
        <ToolbarGroup float="left" style={{ paddingLeft: "0" }}>
          <Button onClick={toggleDrawer("left", true)} className={classes.btns}>
            <MenuIcon />
          </Button>
        </ToolbarGroup>
        <Drawer
          open={state.left}
          onClose={toggleDrawer("left", false)}
          classes={{ paper: classes.drawer }}
        >
          {sideList("left")}
        </Drawer>
        <ToolbarGroup float="left" style={{ paddingLeft: "0" }}>
          <Link to="/" className={classes.homeLink}>
            <Typography variant="h5">VoisiPartage</Typography>
          </Link>
        </ToolbarGroup>
      </Toolbar>
    </AppBar>
  );
}
