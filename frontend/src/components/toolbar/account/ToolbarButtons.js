import React from "react";
import { Avatar, Typography, Tooltip, IconButton } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import DashboardIcon from "@material-ui/icons/Dashboard";
import { Link } from "react-router-dom";

const styles = {
  root: {
    display: "flex",
    alignItems: "center"
  },
  msg: {
    marginRight: 10
  },
  btns: {
    color: "white"
  }
};

class ToolbarButtons extends React.Component {
  state = { isLoggedIn: false };

  toggleLogin = () => {
    this.setState({ isLoggedIn: !this.state.isLoggedIn });
  };

  render() {
    const { classes } = this.props;
    const { isLoggedIn } = this.state;
    return (
      <div>
        {isLoggedIn ? (
          <div className={classes.root}>
            <Tooltip title="Ajouter un objet">
              <Link to="/additem">
                <IconButton size="medium" className={classes.btns}>
                  <AddIcon />
                </IconButton>
              </Link>
            </Tooltip>

            <Tooltip title="Mes objets">
              <Link to="/myitems">
                <IconButton size="medium" className={classes.btns}>
                  <DashboardIcon />
                </IconButton>
              </Link>
            </Tooltip>
            <Button
              size="medium"
              onClick={this.toggleLogin}
              className={classes.btns}
            >
              <Typography variant="subtitle1" className={classes.msg}>
                Bonjour, Albert !
              </Typography>
              <Avatar>AB</Avatar>
            </Button>
          </div>
        ) : (
          <div className={classes.root}>
            <Button
              size="medium"
              onClick={this.toggleLogin}
              className={classes.btns}
            >
              <Typography variant="subtitle1" className={classes.msg}>
                Log in!
              </Typography>
              <AccountCircleIcon className={classes.extendedIcon} />
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(ToolbarButtons);
