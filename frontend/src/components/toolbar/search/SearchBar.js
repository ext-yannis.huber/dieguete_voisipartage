import React from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Paper, IconButton, Divider, TextField } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";

import { withStyles } from "@material-ui/styles";
import SearchEntry from "./SearchEntry";
import data from "../../../data/objects";

class SearchBar extends React.Component {
  static styles = {
    root: {
      padding: "2px 10px 2px 0px",
      display: "flex",
      alignItems: "center",
      width: "100%"
    },
    autocomplete: {
      width: "100%"
    },
    input: {
      width: "100%",
      flex: 1
    },
    iconButton: {
      padding: "2px 4px 2px 4px"
    },
    divider: {
      height: 24,
      margin: 4
    },
    searchResults: {
      position: "absolute"
    }
  };

  state = { searchText: "" };

  handleSearch = e => {
    const {
      target: { value }
    } = e;

    this.setState({ searchText: value });

    if (e.key === "Enter") {
      this.context.router.push("/searchitem");
    }
  };

  render() {
    const { classes } = this.props;
    const { searchText } = this.state;

    return (
      <Paper className={classes.root}>
        <Autocomplete
          className={classes.autocomplete}
          // Limit the number of items to 20
          options={data.slice(0, 20)}
          freeSolo
          disableOpenOnFocus
          onChange={(e, v) => {
            if (v && v.id) {
              this.props.history.push(`/details/${v.id}`);
            }
          }}
          renderInput={params => (
            <TextField
              {...params}
              className={classes.input}
              aria-label="search"
              placeholder="Recherche d'objets"
              onChange={this.handleSearch}
              value={searchText}
              InputProps={{
                ...params.InputProps,
                disableUnderline: true,
                style: { paddingLeft: "10px" }
              }}
            />
          )}
          getOptionLabel={option => option.title}
          renderOption={option => <SearchEntry data={option} />}
        />
        <Divider orientation="vertical" className={classes.divider} />

        <Link to={"/searchitem"}>
          <IconButton aria-label="search" className={classes.iconButton}>
            <SearchIcon />
          </IconButton>
        </Link>
      </Paper>
    );
  }
}

export default withStyles(SearchBar.styles)(withRouter(SearchBar));
