import React from "react";
import { Typography, Chip, Tooltip } from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import ClearIcon from "@material-ui/icons/Clear";
import { withStyles } from "@material-ui/styles";

class SearchEntry extends React.Component {
  static style = {
    root: {
      width: "100%",
      display: "grid",
      gridTemplateRow: "auto auto",
      gridTemplateColumn: "auto 1fr auto"
    },
    itemImg: {
      maxHeight: "60px",
      width: "auto",
      height: "auto",
      marginRight: "10px",
      gridRow: "1 / span 2",
      gridColumn: 1
    }
  };

  render() {
    const { data, classes } = this.props;

    return (
      <div className={classes.root}>
        <img
          src={data.imageUrl[0]}
          alt={data.title}
          className={classes.itemImg}
        />
        <Typography variant="h6" stle={{ gridColumn: 2, gridRow: 1 }}>
          {data.title}
        </Typography>
        <Typography
          variant="subtitle2"
          noWrap={true}
          style={{
            gridColumn: 2,
            gridRow: 2,
            alignSelf: "start"
          }} /*style={{width: "100%"}}*/
        >
          {data.description}
        </Typography>

        <Tooltip
          title={data.availability.isAvailable ? "Disponible" : "Indisponible"}
        >
          <Chip
            label={
              data.availability.isAvailable ? (
                <DoneIcon fontSize={"small"} />
              ) : (
                <ClearIcon fontSize={"small"} />
              )
            }
            //variant="outlined"
            size="small"
            color={data.availability.isAvailable ? "primary" : "secondary"}
            style={{ gridColumn: 3, gridRow: 1 }}
          />
        </Tooltip>
      </div>
    );
  }
}

export default withStyles(SearchEntry.style)(SearchEntry);
