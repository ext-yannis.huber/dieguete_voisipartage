---
title: Séance IHM
created: '2019-09-27T12:28:36.460Z'
modified: '2019-09-27T12:58:31.793Z'
---

# Séance IHM
Pas de Design précis demandé.
Pas de couleur spécifiée, quand même prendre des couleurs qui sont adéquates, écologique, vert?
But de l'application: Encourager les habitants à partager et non pas à acheter.
Pas d'anonymat, l'application sera ouverte à toute une commune, tous les noms et adresses seront publiques.

## Attributs d'un objet
Photo, Titre

Outils ménagers, skis, cuisine, jardin, jouets

Mettre des catégories. Système de Tags. Un objet peut être dans plusieurs catégories.

Durée de prêt maximale

## Spécifications techniques
Application web responsive.
Implémentation d'une messagerie, pouvoir laisser des commentraires.
Option report pour pouvoir signaler des affiches non adéquats.
Implémenter une recherche.

## Publique cible
Tout adultes, possibilité de compter les enfants aussi.
Tous les adultes de la commune ont d'office un compte.

## Fonctionnement d'un prêt
Les produits ne disparaissent pas du site quand ils sont prétés. Une date de rendu sera affichée, ce qui permet à un client de planifier son prêt.
First come first served.
Un article déjà prêté peut être réservé. Un système de liste d'attente est mis en place.
Limite d'objets qu'on peut prêter en même temps.
Si un article n'est pas disponible, des articles similaires sont proposés.

Notion d'indice de confiance, signaler les personnes non respectueux.

Diagramme d'affinité et notes doivent intégrer toutes les notes de la séance.

Les objets sont cherchés par ceux qui veulent emprunter.


## Tests
Appliquer les tests ergonomiques, critiques des equipes adversaires.
