# IHM2 - VoisiPartage

## Notes Yael
Design ? simple et intuitif, pas trop de sous menu  

Couleur ? sérénité, écologique, vie communautaire, énergie, créer des liens  un peu trop tôt pour décider. Encourager les gens à créer des liens dans le quartier, anti-gaspillage

Plateforme ? app web responsive (possible de voir sur mobile), ev PWA

Anonymat ? pas du tout, confiance, collaboration avec commune, que pour la commune mais une communauté prédéfinie (peut être plus large que la commune). La personne est affiliée à un groupe et ne peut pas voir les autres communes.

Objet ? photo, titre, type (ménager, jouet, outillage, etc / liste qui peut être complétée via les propositions des gens/un objet peut avoir plusieurs catégorie) et durée du prêt

Emprunt : durée si plus courte que celle proposée

Si y’a qqch à payer (par exemple essence), ce sera fait de vive voix

Messagerie doit être dispo, commentaires sous l’article

Pas de notes/évaluation mais indice de confiance pour filtrer personnes peu respectueuses (à mettre après la récupération de l’objet) et pour mettre à la personne qui prête

Commune : petite

Pas de modération (plutôt dans le backend)

Objet qu’on prête et qu’on cherche

Plutôt adultes

Un objet emprunté est en cours d’emprunt

Celui qui prête peut donner une durée d’emprunt, après c’est négociable entre les deux personnes

Endommagement : entente entre les deux personnes

Premier qui emprunte peut l’avoir et confirmation du prêteur

Mettre en avant les règles pour les utilisateurs

L’emprunt peut être ultérieur --> inscription sur liste, possibilité de se « réserver » l’objet pour quand il sera restitué

Pas obligé de prêter pour emprunter, pas plus que X objets empruntés à la fois, plateforme propose objets similaires si celui qu’on veut est pas dispo, proposer objet souvent emprunter avec celui qu’on emprunte

Pas besoin d’exposer qui a l’objet

Sur une personne : afficher les objets qu’il prête

Objets interdits : gérés par algo en backend

Prolongement : ok si personne sur la liste d’attente et doit reconfirmer

Possibilité de retirer, désactiver l’objet --> la personne s’engage pour la durée du premier emprunt donc si la personne veut prolonger et que le prêteur veut retirer alors le prolongement n’est pas possible

Elargir pour services avec paiement non géré par la plateforme

Test utilisateur ? en principe on n’a pas le temps mais le groupe adverse critique notre interface

Utilisateur gérer par un compte (on ne gère pas les comptes dans le cadre de ce projet)

Lieu de l’échange : l’emprunteur va chercher ou sinon selon discussion entre les deux personnes --> adresse du prêteur connue
