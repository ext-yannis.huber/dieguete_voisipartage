# IHM2 - VoisiPartage

## Notes
Design simple et intuitif

Couleurs à proposer (ecologie, vie communautaire, création de lien)

But: encourager a ne pas tout acheter (eviter la consommation excessive, créer des liens dans le quartier)

Application WEB responsive souhaité (eventuellement PWA si temps suffisant)

Pas besoins d'anonymat et limité au voisinage/commune

Un objet est mis en avant via une photo, titre, catégorie, temps d'emprunt maximal. Les catégories doivent être dynamiques (modifiables)

Les prêts sont gratuits et les eventuels frais sont gérées en dehors de la plateforme ou sur la messagerie.

Commentaires et messagerie souhaité.

Un système de modération n'est pas nécessaire (eveentuellement la possibilité de report un annonce malvéillante)

Une annonce peut également être une recherche d'objet.

Public cible: tout adulte dans la commune

Un objet reste disponnible en cours d'emprunt

La durée de prêt est défini par la personne qui prête (durée minimale)

Les litiges entre personnes ne sont pas réglés sur la platforme.

Le premier venu premier servi et l'emprunt doit être confirmé.

La réservation d'objet en avance est souhaitée (liste d'attente)

On ne peut pas emprunter plus de x objets a la fois afin de limiter les abus

Si un objet est pris recommendation d'objet similaires

Recommendation en générale

Anonymat des réservations/prêts

Profil par personne des objet prêtés

Tri par disponibilité, proximité

Pas de système de dons, eventuellement caution

Les règele doivent être claires pour les users

Prolongation possible si personne sur la liste d'attente et avec confirmation de la personne qui prête

Désactivation de l'annonce possible

Indice de confiance sur les personnes (pas forcément visible publiquement, permet de refuser un emprunt)

Elargir le projet au services (si le temps le permet)

Tests utilisateurs avec les critères ergonomiques mais pas avec de vrai users

L'objet est a venir chercher chez la personne (adresse à afficher)

