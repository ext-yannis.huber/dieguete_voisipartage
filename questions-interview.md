# Projet IHM2
## Préparation séance client
La complexité de domaine est très faible et la complexité d'interaction est également modérée.

## Questions

### UTILISATEUR
  - Quel est le public cible?
  - L'application doit-elle être limitée au village même ?
  - Quelles sont les données d'un utilisateur qui peuvent être publiques/privées (protection des données d'utilisateurs) ?
  - À quelle fréquence un utilisateur aimerait-il utiliser l'application (hebdomadaire, mensuel, de temps en temps) ?

### MISE EN PAGE - WEB
  - La plateforme est-elle imposée ?
  - Existe-il déjà un design ? Si non, quelles sont les préférences, idées ?
  - Le site doit-il être "responsive" (éventuelle utilisation sur mobile) ?
  - Il y a-t-il une palette de couleurs prédéfinie (par exemple liée à un logo) ? 

### FONCTIONNALITES GENERALES
  - L'utilisateur doit-il pouvoir se géolocaliser pour trouver le matériel le plus proche ? 
  - Un échange peut-il être noté/évalué ? 
  - Doit-il y avoir un moyen pour communiquer entre prêteur et emprunteur (par exemple chat) ?

### PRET/EMPRUNT
  - Comment sont définis un prêt et un emprunt (titre, date, photo, etc) ?
  - Est-ce qu'une photo de l'objet est obligatoire ?
  - Un emprunt peut-il être défini pour une date ultérieure ?
  - Est-il possible de prolonger un emprunt ? 
  - Y a-t-il une modalité prédéfinie pour l'échange (lieu précis, chez la personne qui prête, ...) ?
  - Un emprunt doit-il être confirmé par la personne qui prête ?
  - Le retour d'un objet doit-il également être confirmé ?
  - Que se passe-t-il si un objet est rendu endommagé (éventuellement ranking des utilisateurs) ?
